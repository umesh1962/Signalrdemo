import {  
    NgModule  
} from '@angular/core';  
import {  
    BrowserModule  
} from '@angular/platform-browser';  
import {  
    AppComponent  
} from './app.component';  

import {  
    HttpModule,  
    JsonpModule  
} from '@angular/http';  
import {  
    ClockComponent  
} from './clock.component';  
import {  
    SignalRService  
} from './signalRService';  
import {  
    FormsModule  
} from '@angular/forms';  

import { routing } from './app.route';
@NgModule({  
    imports: [  
        BrowserModule,  
        HttpModule,  
        JsonpModule,  
        FormsModule,
            routing  
    ],  
    declarations: [  
        AppComponent,  
        ClockComponent  
    ],  
    providers: [  
        SignalRService  
    ],  
    bootstrap: [AppComponent]  
})  
export class AppModule {}  