import { ModuleWithProviders }  from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import {  
    ClockComponent  
} from './clock.component';  


// Route Configuration
export const routes: Routes = [
  {
    path: 'clock',
    component:ClockComponent
  },
]
  export const routing: ModuleWithProviders = RouterModule.forRoot(routes);